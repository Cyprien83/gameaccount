﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Calculations : MonoBehaviour {

    public Text goalNumber;
    Text textBtn;

    private Text currentCalcul; 

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void LineCalcul (Text calcul, string operatorChoose)
    {
        // Split string for getting numbers
        char[] charOperator = operatorChoose.ToCharArray();
        string[] LineCalcul = calcul.text.Split(charOperator[0]);
    
        int result;
        int number1 = int.Parse(LineCalcul[0]);
        int number2 = int.Parse(LineCalcul[1]);

        // Switch for operator, if number 1 < number / and - forbidden (in game return 0)
        // Calcul operation calcul
        switch(operatorChoose)
        {
            case "+":
                result = number1 + number2;
                break;
            case "-":
                result = number1 - number2;
                if (number1 < number2)
                {
                    result = 0;
                }
                break;
            case "/":
                if (number1 < number2)
                {
                    result = 0;
                }
                result = number1 / number2;
                break;
            case "x":
                result = number1 * number2;
                break;
            default:
                result = -1;
                break;
        }

        this.displayCalcul(calcul, result);
    }

    private void displayCalcul (Text calcul, int result)
    {
        // Get goal number display victory if reached
        int goalNumberInt = Int32.Parse(goalNumber.text);
        if (goalNumberInt == result)
        {
            this.displayVictory();
        }

        // Get number of current calcul
        string numberCalcul = this.getNumberCalcul(calcul);

        // Enabled egal and computed number for the current calcul and write in the result
        GameObject.Find("egal" + numberCalcul).GetComponent<Text>().enabled = true;
        GameObject.Find("ComputedNumber" + numberCalcul).GetComponent<Image>().enabled = true;
        textBtn = GameObject.Find("ComputedNumber" + numberCalcul).transform.GetChild(0).GetComponent<Text>();
        textBtn.text = result.ToString();
    }

    public void SelectNumber ()
    {

        // Getting the btn and the text
        string NameBtn = EventSystem.current.currentSelectedGameObject.gameObject.transform.name;
        textBtn = GameObject.Find(NameBtn).transform.GetChild(0).GetComponent<Text>();

        // Get a current calcul or set it if null
        if (currentCalcul == null)
        {
            Text calcul = this.GetCurrentCalcul();
            currentCalcul = calcul;
        }
        
        char[] arrayCharCalcul = currentCalcul.text.ToCharArray();

        // Selected number if not the first in calcul, get the lastchar which is the operator
        if (currentCalcul.text.Length > 0)
        {
            string lastChar = arrayCharCalcul[currentCalcul.text.Length - 1].ToString();

            if (lastChar == "x" || lastChar == "/" || lastChar == "+" || lastChar == "-")
            {
                // Disable btn corresponding number
                GameObject.Find(NameBtn).GetComponent<Image>().enabled = false;
                GameObject.Find(NameBtn).GetComponent<Button>().enabled = false;

                // Write in calcul
                currentCalcul.text += textBtn.text;

                // Calcul the operation then set currentCalcul to null for new calcul
                this.LineCalcul(currentCalcul, lastChar);
                this.currentCalcul = null;
                return;
            }
        }

        // If currentCalcul is empty write in and disable btn corresponding number
        if (currentCalcul.text == "")
        {
            GameObject.Find(NameBtn).GetComponent<Image>().enabled = false;
            GameObject.Find(NameBtn).GetComponent<Button>().enabled = false;
            currentCalcul.text += textBtn.text;
        }
    }

    public void SelectOperator ()
    {
        // Getting the btn and the text
        string NameBtn = EventSystem.current.currentSelectedGameObject.gameObject.transform.name;
        textBtn = GameObject.Find(NameBtn).transform.GetChild(0).GetComponent<Text>();
        

        // If no cuurrentCalcul return
        if (currentCalcul == null)
        {
            return;
        }

        // Getting lastchar in calcul
        char[] arrayCharCalcul = currentCalcul.text.ToCharArray();
        string lastChar = arrayCharCalcul[currentCalcul.text.Length - 1].ToString();

        // If lastchar is an oprator return
        if (lastChar == "x" || lastChar == "/" || lastChar == "+" || lastChar == "-")
        {
            return;
        }

        // If reached here and currentCalcul not empty write operator in it
        if (currentCalcul.text.Length != 0)
        {
            currentCalcul.text += textBtn.text;
        }
    }

    private Text GetCurrentCalcul()
    {
        // Get all calculs
        Text[] calculs = GameObject.Find("Calculs").GetComponentsInChildren<Text>();

        // Find calcul empty and return it
        foreach (Text calcul in calculs)
        {
            if (calcul.text == "")
            {
                return calcul;
            }

        }

        return calculs[0];
    }

    private string getNumberCalcul(Text calcul)
    {
        // Return number of current calcul in string
        switch (calcul.name)
        {
            case "Calcul1":
                return "1";
            case "Calcul2":
                return "2";
            case "Calcul3":
                return "3";
            case "Calcul4":
                return "4";
            case "Calcul5":
                return "5";
        }
        return "1";
    }
    private void displayVictory()
    {
        // Get gameObject VictoryModal and setActive it to display 
        // Victory modal
        GameObject VictoryModal = null;
        Transform[] trans = GameObject.Find("Canvas").GetComponentsInChildren<Transform>(true);
        foreach (Transform t in trans)
        {
            if (t.gameObject.name == "VictoryModal")
            {
                VictoryModal = t.gameObject;
            }
        }
        VictoryModal.SetActive(true);


        // Get timer of the game
        GenerateGame ScriptGenerateGame = GameObject.Find("GenerateGame").GetComponent<GenerateGame>();
        ScriptGenerateGame.gameEnded = true;
        String[] Timer = GameObject.Find("GenerateGame").GetComponent<GenerateGame>().timerGame.ToString().Split('.');

        // Display timer in board in victory modal
        Char[] TimerLessSecond = Timer[1].ToCharArray();
        GameObject.Find("Timer").GetComponent<Text>().text = Timer[0] + "." + TimerLessSecond[0] + "s";
    }
}