﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateGame : MonoBehaviour {

    public bool gameEnded;
    public float timerGame = 00.0f;
    private int goalNumber = 0;
    List<int> numbersChoose;
    int goalNumberChoose;

    // Use this for initialization
    void Start () {
        // First launch start the game
        this.startGame();
	}
	
	// Update is called once per frame
	void Update () {
        
        // Update timer
        if (gameEnded == false)
        {
            timerGame += Time.deltaTime;
        }
        
    }

    public void startGame ()
    {
        // Reset all calculations and timer
        this.ResetCalculations();
        this.timerGame = 0.00f;
        this.gameEnded = false;

        goalNumber = 0;
        List<int> numbers = this.generateNumbersForTheGame();
        // Goalnumber should be more than 50 whereas it's to easy 
        while (goalNumber <= 500 || goalNumber > 5000)
        {
            goalNumber = this.generateGoalNumber(numbers);
        }

        // Set numbers and result found, and display it
        this.numbersChoose = numbers;
        this.goalNumberChoose = goalNumber;
        this.displayGeneratedGame();
    }

    private List<int> generateNumbersForTheGame ()
    {
        List<int> numbers = new List<int>();

        // Generate 4 random number beetween 1 and 10
        System.Random rnd = new System.Random();
        int number1 = rnd.Next(1,11);
        int number2 = rnd.Next(1,11);
        int number3 = rnd.Next(1,11);
        int number4 = rnd.Next(1,11);

        // Make a list of easy big numbers to simplify the game
        List<int> bigNumbers = new List<int>();
        bigNumbers.Add(20);
        bigNumbers.Add(25);
        bigNumbers.Add(50);
        bigNumbers.Add(75);
        bigNumbers.Add(100);

        // Get 2 number in bigNumbers
        int number5 = bigNumbers[rnd.Next(5)];
        bigNumbers.RemoveAll(number => number == number5);
        int number6 = bigNumbers[rnd.Next(4)];

        numbers.Add(number1);
        numbers.Add(number2);
        numbers.Add(number3);
        numbers.Add(number4);
        numbers.Add(number5);
        numbers.Add(number6);

        return numbers;
    }

    private int generateGoalNumber (List<int> numbers)
    {
        int goalNumber = 1;
        string operatorRnd;
        List<int> cloneNumbers = new List<int>(numbers); 

        // List of operator avaible
        List<string> operators = new List<string>();
        operators.Add("+");
        operators.Add("/");
        operators.Add("-");
        operators.Add("*");

        // Get a random operator and a random number 5 times to get the goal number
        System.Random rnd = new System.Random();

        for (int i = 0; i < 5; i++)
        {
            // Get one number
            int number1 = cloneNumbers[rnd.Next(cloneNumbers.Count)];
            cloneNumbers.Remove(number1);

            // Get a second number

            int number2 = cloneNumbers[rnd.Next(cloneNumbers.Count)];
            cloneNumbers.Remove(number2);

            // Get the operator, and do an opeartion
            // If number 1 < number 2 / and - are not possible, division by 0 not possible too
            // After operation add result to numbers
            // When i = 5 final number is the goal number
            operatorRnd = operators[rnd.Next(4)];

            if (number1 < number2 || number2 == 0)
            {
                cloneNumbers.Add(number1);
                cloneNumbers.Add(number2);
                i--;
            } else
            {
                int newNumber = 0;

                switch (operatorRnd)
                {
                    case "+":
                        newNumber = number1 + number2;
                        cloneNumbers.Add(newNumber);
                        break;
                    case "-":
                        newNumber = number1 - number2;
                        cloneNumbers.Add(newNumber);
                        if (newNumber == 0)
                        {
                            cloneNumbers.Add(number1);
                            cloneNumbers.Add(number2);
                            i--;
                            break;
                        }
                        break;
                    case "/":
                        newNumber = number1 / number2;
                        cloneNumbers.Add(newNumber);
                        break;
                    case "*":
                        newNumber = number1 * number2;
                        cloneNumbers.Add(newNumber);
                        break;
                    default:
                        newNumber = number1 + number2;
                        cloneNumbers.Add(newNumber);
                        break;
                }

                if (i == 4)
                {
                    goalNumber = newNumber;
                }
            }
        }

        return goalNumber;
    }

    private void displayGeneratedGame ()
    {
        // Display list of 6 numbers in btn
        int i = 0;
        foreach (int number in this.numbersChoose)
        {
            int index = i + 1;
            string btnWanted = "Number" + index.ToString();
            GameObject.Find(btnWanted).GetComponentInChildren<Text>().text = this.numbersChoose[i].ToString();
            i++;
        }

        // Display the goal number
        GameObject.Find("GoalNumber").GetComponentInChildren<Text>().text = this.goalNumberChoose.ToString();
    }

    public void ResetCalculations()
    {
        // Get all the calculs, erase them
        Text[] calculs = GameObject.Find("Calculs").GetComponentsInChildren<Text>();
        foreach (Text calcul in calculs)
        {
            calcul.text = "";
        }

        // Get all the egals, erase them
        Text[] egals = GameObject.Find("Egals").GetComponentsInChildren<Text>();
        foreach (Text egal in egals)
        {
            egal.enabled = false;
        }

        // Get all the computed numbers, erase them
        Button[] computedNumbers = GameObject.Find("ComputedNumbers").GetComponentsInChildren<Button>();
        foreach (Button computedNumber in computedNumbers)
        {
            computedNumber.transform.GetChild(0).GetComponent<Text>().text = "";
            computedNumber.GetComponent<Image>().enabled = false;
        }

        // Get all the numbers, make them active
        Button[] numbers = GameObject.Find("Numbers").GetComponentsInChildren<Button>();
        foreach (Button number in numbers)
        {
            number.GetComponent<Image>().enabled = true;
            number.GetComponent<Button>().enabled = true;
        }
    }

    public void PlayAgain ()
    {
        // For a new game reset calcultaions, restart game and disable victory modal
        this.ResetCalculations();
        this.startGame();

        // Get victory modal and disable it
        GameObject VictoryModal = null;
        Transform[] trans = GameObject.Find("Canvas").GetComponentsInChildren<Transform>(true);
        foreach (Transform t in trans)
        {
            if (t.gameObject.name == "VictoryModal")
            {
                VictoryModal = t.gameObject;
            }
        }
        VictoryModal.SetActive(false);
    }
}
